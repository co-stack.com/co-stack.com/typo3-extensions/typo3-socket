# CoStack.Typo3Socket

## Disclaimer

This project was created as a research project into TCP sockets and the implementation possibilities with TYPO3.

**USE THIS PACKAGE ONLY FOR EDUCATIONAL PURPOSES.**

Running the TYPO3 socket will allow anyone with access to the TCP port to create any record in your TYPO3, even Admin
Backend Users. There is no security check in place to prevent malicious actions.

## What's this?

This TYPO3 extension brings the wonderful world of TCP Sockets to your TYPO3.

You can use this extension in two possible configurations.

* Passive: For the passive mode, you start the socket server on the CLI. It will create a listener which other clients
  can connect to.
* Active: In active mode, no socket listener is created. But every DataMap handled by the DataHandler will be sent to
  the configured passive socket listener. This replicates every action done on the active side on the passive side.

## Installation

`composer require co-stack/typo3-socket`

## Usage

After installation and activation you can start the socket on the command line with following
command: `bin/typo3 socket:run`

Open another bash and type `telnet 127.0.0.1 8800`, now you have an interactive TYPO3 console. Press enter for command
overview.
Open a bunch of additional CLIs and connect them with the telnet command, too. Try commands like `clients`, `exit` or
write a message to all connected clients via `broadcast:Hello Clients!`.
You can also access data from the TYPO3 system. Currently, this is limited to page properties. Type `show:id:1` to get
all page properties of the page with UID 1.

## Documentation

TDB

## Use cases

* Research & Education
* Interactive code execution in TYPO3 context
* Access to TYPO3 database without knowing credentials

## Future Features

* Unix socket
* Login with backend account
* Manage data like users and groups
* runs in background, socket will be assured to be running by cronjob
* much, much more
