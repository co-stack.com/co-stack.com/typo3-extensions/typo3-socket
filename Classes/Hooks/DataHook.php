<?php

declare(strict_types=1);

namespace CoStack\Typo3Socket\Hooks;

use Exception;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\DataHandling\DataHandler;

use function explode;
use function fsockopen;
use function fwrite;
use function strpos;
use function substr;

use const PHP_EOL;

readonly class DataHook
{
    /**
     * @var array{host: string, port: int, passive: bool}
     */
    protected array $extConf;

    public function __construct(ExtensionConfiguration $extensionConfiguration)
    {
        $this->extConf = $extensionConfiguration->get('typo3_socket');
    }

    public function processDatamap_afterAllOperations(DataHandler $dataHandler)
    {
        $uri = $this->extConf['uri'];

        $scheme = 'tcp';
        $pos = strpos($uri, '://');
        if ($pos !== false) {
            $scheme = substr($uri, 0, $pos);
        }

        try {
            if ($scheme === 'unix') {
                $handle = fsockopen($uri);
            } else {
                [$ip, $port] = explode(':', $uri);
                $handle = fsockopen($ip, (int)$port);
            }

            if (!empty($dataHandler->datamap)) {
                $dataString = 'dh:data:' . json_encode($dataHandler->datamap);
                fwrite($handle, $dataString . PHP_EOL);
            }
            fclose($handle);
            unset($handle);
        } catch (Exception $exception) {
            // ignore
        }
    }
}
