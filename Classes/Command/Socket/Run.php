<?php

declare(strict_types=1);

namespace CoStack\Typo3Socket\Command\Socket;

use CoStack\Typo3Socket\Server\Factory\ServerFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;

class Run extends Command
{
    public function __construct(
        private readonly ExtensionConfiguration $extensionConfiguration,
        private readonly ServerFactory $serverFactory,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $extConf = $this->extensionConfiguration->get('typo3_socket');
        try {
            $server = $this->serverFactory->create($output, $extConf['uri']);
            $server->start();
        } catch (Throwable $exception) {
            $output->writeln((string)$exception);
            return self::FAILURE;
        }
        return self::SUCCESS;
    }
}
