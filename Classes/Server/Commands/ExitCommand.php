<?php

namespace CoStack\Typo3Socket\Server\Commands;

use CoStack\Typo3Socket\Server\Client;
use CoStack\Typo3Socket\Server\Server;

class ExitCommand implements Command
{
    public function isHidden(): bool
    {
        return false;
    }

    public function getName(): string
    {
        return 'exit';
    }

    public function getDescription(): string
    {
        return 'Terminates your connection';
    }

    public function execute(Server $server, Client $client, string $clientInput): int
    {
        $client->disconnect('Goodbye');
        return 0;
    }
}
