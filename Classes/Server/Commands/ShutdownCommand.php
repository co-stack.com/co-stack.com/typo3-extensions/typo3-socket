<?php

declare(strict_types=1);

namespace CoStack\Typo3Socket\Server\Commands;

use CoStack\Typo3Socket\Server\Client;
use CoStack\Typo3Socket\Server\Server;

use function sprintf;

class ShutdownCommand implements Command
{
    public function isHidden(): bool
    {
        return false;
    }

    public function getName(): string
    {
        return 'shutdown';
    }

    public function getDescription(): string
    {
        return 'Stop the TYPO3 socket server';
    }

    public function execute(Server $server, Client $client, string $clientInput): int
    {
        $server->output->writeln(
            sprintf(
                'Received shutdown signal from client %s from %s on %s',
                $client->getId(),
                $client->getRemoteAddress(),
                $client->getLocalAddress(),
            ),
        );
        $server->shutdown('Server shutdown');
        return 0;
    }

}
