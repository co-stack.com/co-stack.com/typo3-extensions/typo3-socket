<?php

namespace CoStack\Typo3Socket\Server\Commands;

use CoStack\Typo3Socket\Server\Client;
use CoStack\Typo3Socket\Server\Server;

interface Command
{
    public function isHidden(): bool;

    public function getName(): string;

    public function getDescription(): string;

    public function execute(Server $server, Client $client, string $clientInput): int;
}
