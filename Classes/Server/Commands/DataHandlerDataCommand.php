<?php

declare(strict_types=1);

namespace CoStack\Typo3Socket\Server\Commands;

use CoStack\Typo3Socket\Server\Client;
use CoStack\Typo3Socket\Server\Server;
use JsonException;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\Utility\GeneralUtility;

use function json_decode;
use function sprintf;

class DataHandlerDataCommand implements Command
{
    public function isHidden(): bool
    {
        return false;
    }

    public function getName(): string
    {
        return 'dh:data:';
    }

    public function getDescription(): string
    {
        return 'Stream data to the DataHandler. Example to create a new Content Element on PID 1: "dh:data:{"tt_content":{"NEW1234":{"pid":1,"header":"Hello World"}}}"';
    }

    public function execute(Server $server, Client $client, string $clientInput): int
    {
        $server->output->writeln(
            sprintf(
                'Client %s from %s posted data on %s.',
                $client->getId(),
                $client->getRemoteAddress(),
                $client->getLocalAddress(),
            ),
        );
        $GLOBALS['BE_USER']->user['uid'] = 1;
        $GLOBALS['BE_USER']->user['admin'] = 1;
        $GLOBALS['BE_USER']->workspace = 0;
        try {
            $array = json_decode($clientInput, true, 512, JSON_THROW_ON_ERROR);
        } catch (JsonException $exception) {
            $client->write('Invalid JSON input: ' . $exception->getMessage());
            return 1;
        }

        $dataHandler = GeneralUtility::makeInstance(DataHandler::class);
        $dataHandler->start($array, []);
        $dataHandler->process_datamap();

        $client->write('Done');
        return 0;
    }

}
