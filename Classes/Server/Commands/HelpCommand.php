<?php

namespace CoStack\Typo3Socket\Server\Commands;

use CoStack\Typo3Socket\Server\Client;
use CoStack\Typo3Socket\Server\CommandRegistry;
use CoStack\Typo3Socket\Server\Server;
use TYPO3\CMS\Core\Utility\GeneralUtility;

use function str_pad;
use function strlen;

class HelpCommand implements Command
{
    public function isHidden(): bool
    {
        return false;
    }

    public function getName(): string
    {
        return 'help';
    }

    public function getDescription(): string
    {
        return 'Prints this help';
    }

    public function execute(Server $server, Client $client, string $clientInput): int
    {
        $commandRegistry = GeneralUtility::makeInstance(CommandRegistry::class);
        $client->write('Help');
        $client->write('');
        $client->write('Available Commands:');

        $longestName = 0;
        $commandsList = [];
        foreach ($commandRegistry->getCommands() as $command) {
            if ($command->isHidden()) {
                continue;
            }
            $name = $command->getName();
            $nameLength = strlen($name);
            if ($nameLength > $longestName) {
                $longestName = $nameLength;
            }
            $commandsList[$name] = $command->getDescription();
        }
        $paddingLength = $longestName + 4;
        foreach ($commandsList as $name => $description) {
            $client->write('    ' . str_pad($name, $paddingLength) . $description);
        }
        return 0;
    }
}
