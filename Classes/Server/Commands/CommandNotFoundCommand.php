<?php

declare(strict_types=1);

namespace CoStack\Typo3Socket\Server\Commands;

use CoStack\Typo3Socket\Server\Client;
use CoStack\Typo3Socket\Server\CommandRegistry;
use CoStack\Typo3Socket\Server\Server;
use TYPO3\CMS\Core\Utility\GeneralUtility;

use function escapeshellcmd;
use function sprintf;

class CommandNotFoundCommand implements Command
{
    public function __construct(private readonly string $helpCommand)
    {
    }

    public function isHidden(): bool
    {
        return true;
    }

    public function getName(): string
    {
        return 'command_not_found';
    }

    public function getDescription(): string
    {
        return '';
    }

    public function execute(Server $server, Client $client, string $clientInput): int
    {
        $client->write(sprintf('The command "%s" could not be found.', escapeshellcmd($clientInput)));
        $commandRegistry = GeneralUtility::makeInstance(CommandRegistry::class);
        $commands = $commandRegistry->getCommands();
        if (isset($commands[$this->helpCommand])) {
            return $commands[$this->helpCommand]->execute($server, $client, '');
        }
        return 1;
    }
}
