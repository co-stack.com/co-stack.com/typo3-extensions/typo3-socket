<?php

declare(strict_types=1);

namespace CoStack\Typo3Socket\Server\Commands;

use CoStack\Typo3Socket\Server\Client;
use CoStack\Typo3Socket\Server\Server;

use function count;

class ClientsCommand implements Command
{
    public function isHidden(): bool
    {
        return false;
    }

    public function getName(): string
    {
        return 'clients';
    }

    public function getDescription(): string
    {
        return 'Show the number of currently connected clients';
    }

    public function execute(Server $server, Client $client, string $clientInput): int
    {
        $client->write('Number of active clients: ' . count($server->getClients()));
        return 0;
    }

}
