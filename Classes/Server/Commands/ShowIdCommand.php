<?php

declare(strict_types=1);

namespace CoStack\Typo3Socket\Server\Commands;

use CoStack\Typo3Socket\Server\Client;
use CoStack\Typo3Socket\Server\Server;
use TYPO3\CMS\Core\Database\ConnectionPool;

use function print_r;

class ShowIdCommand implements Command
{
    private ConnectionPool $connectionPool;

    public function __construct(ConnectionPool $connectionPool)
    {
        $this->connectionPool = $connectionPool;
    }

    public function isHidden(): bool
    {
        return false;
    }

    public function getName(): string
    {
        return 'show:id:';
    }

    public function getDescription(): string
    {
        return 'Show all properties of the page with UID X';
    }

    public function execute(Server $server, Client $client, string $clientInput): int
    {
        $uid = (int)$clientInput;

        $queryBuilder = $this->connectionPool->getQueryBuilderForTable('pages');
        $queryBuilder
            ->select('*')
            ->from('pages')
            ->where($queryBuilder->expr()->eq('uid', $uid))
            ->setMaxResults(1);
        $result = $queryBuilder->executeQuery();
        $row = $result->fetchAssociative();
        if (false === $row) {
            $client->write('Page with UID [' . $uid . '] not found');
            return 1;
        }

        $client->write('Showing page properties of page [' . $uid . ']');
        $client->write(print_r($row, true));
        return 0;
    }
}
