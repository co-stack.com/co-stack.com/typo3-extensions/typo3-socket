<?php

declare(strict_types=1);

namespace CoStack\Typo3Socket\Server\Commands;

use CoStack\Typo3Socket\Server\Client;
use CoStack\Typo3Socket\Server\Server;

use function escapeshellcmd;
use function trim;

class BroadcastCommand implements Command
{
    public function isHidden(): bool
    {
        return false;
    }

    public function getName(): string
    {
        return 'broadcast:';
    }

    public function getDescription(): string
    {
        return 'send a message X to all connected clients';
    }

    public function execute(Server $server, Client $client, string $clientInput): int
    {
        $message = escapeshellcmd(trim($clientInput));
        foreach ($server->getClients() as $otherClient) {
            if ($client !== $otherClient) {
                $otherClient->write('');
                $otherClient->write('T3S BROADCAST: ' . $message);
            }
        }
        return 0;
    }

}
