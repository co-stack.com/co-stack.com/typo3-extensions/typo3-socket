<?php

namespace CoStack\Typo3Socket\Server;

use CoStack\Typo3Socket\Stream\BufferedStream;
use CoStack\Typo3Socket\Stream\Stream;
use Evenement\EventEmitterTrait;
use React\Socket\ConnectionInterface;

use function spl_object_hash;

class Client
{
    use EventEmitterTrait;

    protected Stream $stream;

    public function __construct(private ConnectionInterface $connection)
    {
        $this->stream = new BufferedStream($connection);
        $this->stream->on('data', $this->emitData(...));
    }

    public function disconnect(string $message): void
    {
        $this->emit('disconnect', [$this]);
        $this->stream->end($message);
    }

    private function emitData(string $data): void
    {
        $this->emit('data', [$this, $data]);
    }

    public function getId(): string
    {
        return spl_object_hash($this->stream);
    }

    public function getRemoteAddress(): string
    {
        return $this->connection->getRemoteAddress();
    }

    public function getLocalAddress(): string
    {
        return $this->connection->getLocalAddress();
    }

    public function write(string $text, bool $newline = true): void
    {
        if ($newline) {
            $text .= "\n";
        }
        $this->stream->write($text);
    }
}
