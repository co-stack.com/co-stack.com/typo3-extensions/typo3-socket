<?php

declare(strict_types=1);

namespace CoStack\Typo3Socket\Server\Factory;

use CoStack\Typo3Socket\Server\CommandRegistry;
use CoStack\Typo3Socket\Server\Server;
use React\Socket\SocketServer;
use Symfony\Component\Console\Output\OutputInterface;

class ServerFactory
{
    public function __construct(
        private readonly CommandRegistry $commandRegistry,
    ) {
    }

    public function create(OutputInterface $output, string $uri): Server
    {
        $socketServer = new SocketServer($uri);
        return new Server($output, $this->commandRegistry->getCommands(), $socketServer);
    }
}
