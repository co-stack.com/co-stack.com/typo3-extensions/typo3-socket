<?php

namespace CoStack\Typo3Socket\Server;

use CoStack\Typo3Socket\Server\Commands\Command;

use function strlen;
use function uksort;

class CommandRegistry
{
    /**
     * @param array<Command> $commands
     */
    public function __construct(private readonly iterable $commands)
    {
    }

    /**
     * @return array<string, Command>
     */
    public function getCommands(): array
    {
        $commands = [];
        foreach ($this->commands as $command) {
            $commands[$command->getName()] = $command;
        }
        uksort($commands, static fn($a, $b): int => strlen($b) - strlen($a));
        return $commands;
    }
}
