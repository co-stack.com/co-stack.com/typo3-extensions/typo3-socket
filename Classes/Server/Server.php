<?php

namespace CoStack\Typo3Socket\Server;

use CoStack\Typo3Socket\Server\Commands\Command;
use React\Socket\ConnectionInterface;
use React\Socket\SocketServer;
use Symfony\Component\Console\Output\OutputInterface;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

use function count;
use function escapeshellarg;
use function func_get_args;
use function sprintf;
use function strlen;
use function substr;

class Server
{
    /**
     * @var array<Client>
     */
    protected array $clients = [];

    /**
     * @param OutputInterface $output
     * @param array<string, Command> $commands
     * @param SocketServer $socketServer
     */
    public function __construct(
        public readonly OutputInterface $output,
        private readonly array $commands,
        private readonly SocketServer $socketServer,
    ) {
    }

    public function start(): void
    {
        $this->socketServer->on('connection', $this->onConnection(...));
        $this->output->writeln('Socket established on ' . $this->socketServer->getAddress());
    }

    private function onConnection(ConnectionInterface $connection): void
    {
        $client = new Client($connection);
        $this->clients[$client->getId()] = $client;

        $version = ExtensionManagementUtility::getExtensionVersion('typo3_socket');
        $this->output->writeln(
            sprintf(
                'New connection %s from %s on %s. %d clients are connected.',
                $client->getId(),
                $client->getRemoteAddress(),
                $client->getLocalAddress(),
                count($this->clients),
            ),
        );
        $client->write(sprintf('TYPO3 Socket version %s', $version));
        $client->write('  Copyright (C) 2022 Oliver Eglseder <oliver.eglseder@co-stack.com>');
        $client->write('  TYPO3 Socket comes with ABSOLUTELY NO WARRANTY;');
        $client->write('  This is free software, and you are welcome to redistribute it under certain conditions');
        $client->write('  See the LICENSE file of this package for more information.');
        $client->on('data', $this->onClientData(...));
        $client->on('disconnect', $this->onClientDisconnect(...));
        $client->on('close', $this->onClientClose(...));
        $client->on('end', $this->onClientEnd(...));
    }

    private function onClientClose()
    {
        $args = func_get_args();
    }
    private function onClientEnd()
    {
        $args = func_get_args();
    }

    private function onClientData(Client $client, string $data): void
    {
        $this->output->writeln(
            sprintf(
                'Received data "%s" from client %s on stream %s on %s.',
                escapeshellarg($data),
                $client->getId(),
                $client->getRemoteAddress(),
                $client->getLocalAddress(),
            ),
            OutputInterface::VERBOSITY_VERBOSE,
        );

        if ('' !== $data) {
            foreach ($this->commands as $prefix => $command) {
                if (str_starts_with($data, $prefix)) {
                    $command->execute($this, $client, substr($data, strlen($prefix)));
                    return;
                }
            }
        }
        $this->commands['command_not_found']->execute($this, $client, $data);
    }

    private function onClientDisconnect(Client $client): void
    {
        unset($this->clients[$client->getId()]);
        $this->output->writeln(
            sprintf(
                'Client %s from %s disconnected from %s. %d clients are connected.',
                $client->getId(),
                $client->getRemoteAddress(),
                $client->getLocalAddress(),
                count($this->clients),
            ),
        );
    }

    public function shutdown(string $message = 'Server shutting down'): void
    {
        foreach ($this->clients as $client) {
            $client->disconnect($message);
        }
        $this->socketServer->close();
    }

    public function getClients(): array
    {
        return $this->clients;
    }
}
