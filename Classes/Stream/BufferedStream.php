<?php

declare(strict_types=1);

namespace CoStack\Typo3Socket\Stream;

use Evenement\EventEmitterTrait;
use Exception;
use React\Socket\ConnectionInterface;

use function array_pop;
use function chr;
use function implode;
use function str_split;

class BufferedStream implements Stream
{
    use EventEmitterTrait;

    protected string $buffer = '';

    public function __construct(private readonly ConnectionInterface $connection)
    {
        $this->connection->on('data', [$this, 'onData']);
    }

    /**
     * @throws Exception
     */
    public function onData(string $data): void
    {
        $this->buffer .= $data;
        while (str_contains($this->buffer, "\n")) {
            [$sequence, $this->buffer] = explode("\n", $this->buffer, 2);
            $sequence = $this->resolveBackspaces($sequence);
            $this->emit('data', [trim($sequence), $this]);
        }
    }

    protected function resolveBackspaces(string $data): string
    {
        $backspace = chr(8);

        $result = [];
        $strChars = str_split($data);
        foreach ($strChars as $chr) {
            if ($backspace === $chr) {
                array_pop($result);
                continue;
            }
            $result[] = $chr;
        }
        return implode('', $result);
    }

    public function writeLine(string $data): void
    {
        $this->connection->write(str_replace("\n", "\r\n", $data) . "\r\n");
    }

    public function write(string $data): void
    {
        $this->connection->write(str_replace("\n", "\r\n", $data));
    }

    public function end(string $data = null): void
    {
        $this->connection->end(str_replace("\n", "\r\n", $data) . "\r\n");
    }
}
