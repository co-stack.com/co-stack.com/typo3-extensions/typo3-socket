<?php

/**
 * @noinspection PhpMissingStrictTypesDeclarationInspection
 */

use CoStack\Typo3Socket\Hooks\DataHook;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Utility\GeneralUtility;

if (!GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('typo3_socket', 'passive')) {
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass'][] = DataHook::class;
}
